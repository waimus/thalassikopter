# Thalassikopter

### Technical Info:
* Godot Engine version 3.4
* GDScript
* OpenGL 3
* Linux x86_64, Windows 64-bit, macOS

### Licensing
Thalassikopter is open-source, licensed under the terms of the Mozilla Public License version 2.0, see [LICENSE](./LICENSE). 
Game assets are licensed under the [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) license terms.

Third-party contents may have the license file included it its directory accompanying associated assets.

### Instruction:
* At least needed to be opened with the Godot editor once to import/processing the assets.
* Linux, Windows, macOS builds are available at https://waimus.itch.io/thalassikopter

### Working:
* Moves with mouse or keyboard (better with mouse)
* First-person pilot view
* Freelook camera on third person and first person mode
* Engine power
* Helicopter HUD

### In Progress/Planning:
* Helicopter model & animation
* Helicopter cockpit model
* Sounds
* Gameplay, objective, level and basically the whole thing that makes a game

### Known Issues:
Issues are documented on [the issue page](https://gitlab.com/waimus/thalassikopter/-/issues)

#### This however, is some issue that used to occur but not so well documented:
* macOS build is untested (no hardware, no way to test)
* Windows build is having broken camera freelook toggle when running via WINE (caffe/bottles)

### Fixed Known Issues:
* (FIXED) The `$RotationHelper` isn't moving when both keyboard and mouse is used/active (input)
* (FIXED) Helicopter rotation follows the camera when exitting (lerping back) to default rotation
* (FIXED) Helicopter rotation does not follow angular_velocity if pedal gives input during freelook
* (FIXED) Raycast direction messed up when tried to land on a sharp corner or crashing at high speed
* (FIXED) Ground raycast check isn't working
